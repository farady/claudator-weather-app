## Build

Clone this repo and then you can build the project with lein

    $ lein uberjar

## Usage

Run without building

    $ lein run

Get usage help

    $ lein run claudator.core -h
    
